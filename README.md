# GraphQL Security

Protect GraphQL from DoS attacks. Implement authentication & authorization for GraphQL endpoint.


![](images/high-level-architecture-diagram.png)